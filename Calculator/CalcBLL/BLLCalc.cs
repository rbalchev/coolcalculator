﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLL
{
    public class BLLCalc
    {
        public static string DoSum(string[] parms)
        {
            int result = 0;
            foreach (var item in parms)
            {
                int num = 0;
                Int32.TryParse(item, out num);
                result += num;
            }

            return result.ToString();
        }
        public static string DoSubstract(string[] parms)
        {
            int result = 0;
            foreach (var item in parms)
            {
                int num = 0;
                Int32.TryParse(item, out num);
                result += num;
            }

            return result.ToString();
        }
    }
}
